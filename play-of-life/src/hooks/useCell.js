import { useContext, useEffect, useState } from 'react';

// Constants
import { STATUS_CELL } from '../constants';

// Contexts
import { BoardContext } from '../contexts/board.context';

// Helpers
import { checkStatusCell } from '../helpers';

export const useCell = (row, col) => {

    const { cells, setCells, started, count } = useContext(BoardContext);
    const [cell, setCell] = useState(cells[row][col]);
    const [color, setColor] = useState({ backgroundColor: 'gray' });

    // Obtiene las células que rodean a la célula actual
    const getCellReferences = () => {

        const references = [];
        for (const ref of cell.references) {
            
            references.push(cells[ref.row][ref.col]);
        }
        
        return references;
    };

    // Devuelve el estado inverso de la célula actual
    const inverseStatus = (currentCell) => {

        return (currentCell.status === STATUS_CELL.ALIVE)
                ? STATUS_CELL.DEAD: STATUS_CELL.ALIVE;
    }

    // Cambia el estado de la célula
    const changeStatus = () => {

        const cellsMap = cells.map(c => c);
        const currentCell = cellsMap[row][col];
        currentCell.status = inverseStatus(currentCell);
        setCells(cellsMap);
        resolveColor(currentCell);
    }

    // Resuelve el color de la célula de acuerdo a su estado actual
    const resolveColor = (currentCell) => {

        setColor((currentCell.status === STATUS_CELL.ALIVE)
                    ? { backgroundColor: 'green' }
                    : { backgroundColor: 'gray' }
                );
    };

    // Chequea el estado de la célula
    useEffect(() => {
      
        if ( started ) {

            checkStatusCell(cell, setCell, getCellReferences());
        }

    }, [started, count]);

    return {
        cell,
        setCell,
        changeStatus,
        color,
        resolveColor
    }

}
