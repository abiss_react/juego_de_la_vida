import React from 'react';
import ReactDOM from 'react-dom';
import PlayOfLifeApp from './PlayOfLifeApp';

import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(
  <React.StrictMode>
    <PlayOfLifeApp />
  </React.StrictMode>,
  document.getElementById('root')
);
