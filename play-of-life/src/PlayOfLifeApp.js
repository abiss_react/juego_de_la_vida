import { Board } from './components';
import './PlayOfLifeApp.css';

function PlayOfLifeApp() {
  return (
    <div>
        <Board />    
    </div>
  );
}

export default PlayOfLifeApp;
