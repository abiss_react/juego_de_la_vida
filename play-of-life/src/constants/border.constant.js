export const BORDER = {
    
    NONE: 'None',
    TOP: 'top',
    LEFT: 'left',
    RIGHT: 'right',
    BOTTOM: 'bottom'
}