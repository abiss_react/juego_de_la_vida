// Constants
import { CORNER, BORDER, STATUS_CELL } from "../constants";

/**
 * Crea una matriz de 2 dimensiones, creando copias del elemento que llenará cada celda
 * @param {Object} element Elemento que se guardará en cada celda
 * @param {*} rows Cantidad de filas
 * @param {*} cols Cantidad de columnas
 * @returns Matriz generada
 */
export const fillArray = (rows = 10, cols = 10) => {

    const array = [];
    for (let i = 0; i < rows; i++) {

        for (let j = 0; j < cols; j++) {

            const cell = {
                id: `${i}-${j}`,
                row: i,
                col: j,
                status: STATUS_CELL.DEAD
            };

            setCorner(cell, i, j, rows, cols);
            setBorder(cell, i, j, rows, cols);
            setAroundReferencesOfCell(cell, rows, cols);
            setAroundReferencesOfCellCorner(cell, rows, cols);
            setAroundReferencesOfCellBorder(cell, rows, cols);

            array.push({...cell});
        }
    }

    return buildMatriz(array, cols);
};

/**
 * Construye la matriz de 2 dimensiones a partir de un array
 * @param {*} array Array con los elementos para la matriz
 * @param {*} countCols Cantidad de columnas para la matriz resultante
 * @returns Array de array que representa la matriz
 */
export const buildMatriz = (cells = [], countCols = 1) => {

    let newArray = [];
    for (let i = 0; i < cells.length; i += countCols) {

        newArray.push(cells.slice(i, i + countCols));
    }

    return newArray;
}

/**
 * Estable la propiedad corner para las celdas que corresponden a cada corner de la matriz
 * @param {Object} cell Objeto que representa los datos de una celda
 * @param {Number} row Número de fila a la que pertenece la celda
 * @param {Number} col Número de columna a la que pertenece la celda
 * @param {Number} rows Cantidad de filas de la matriz
 * @param {Number} cols Cantidad de columnas de la matriz
 */
export const setCorner = (cell, row, col, rows, cols) => {

    // Corner top-left
    if ( row === 0 && col === 0 ) {

        cell.corner = true;
        cell.cornerType = CORNER.TOP_LEFT;
        return;
    }

    // Corner top-right
    if ( row === 0 && col === cols - 1 ) {
        
        cell.corner = true;
        cell.cornerType = CORNER.TOP_RIGHT;
        return;
    }

    // Corner bottom-left
    if ( row === rows - 1 && col === 0 ) {
        
        cell.corner = true;
        cell.cornerType = CORNER.BOTTOM_LEFT;
        return;
    }

    // Corner bottom-right
    if ( row === rows - 1 && col === cols - 1 ) {
        
        cell.corner = true;
        cell.cornerType = CORNER.BOTTOM_RIGHT;
        return;
    }

    cell.corner = false;
    cell.cornerType = CORNER.NONE;
}

/**
 * Estable la propiedad border para las celdas que corresponden a cada borde de la matriz
 * @param {Object} cell Objeto que representa los datos de una celda
 * @param {Number} row Número de fila a la que pertenece la celda
 * @param {Number} col Número de columna a la que pertenece la celda
 * @param {Number} rows Cantidad de filas de la matriz
 * @param {Number} cols Cantidad de columnas de la matriz
 */
 export const setBorder = (cell, row, col, rows, cols) => {

    // border top || corner top-left
    if ( (row === 0 || (row === 0 && col === 0))
        // Border top || Corner top-right
        || row === 0 || (row === 0 && col === cols - 1)) {

        cell.border = true;
        cell.borderType = BORDER.TOP;
        return;
    }

    // Border bottom || Corner bottom-left
    if ( (row === rows - 1 || (row === rows - 1 && col === 0))
        // Border bottom || Corner bottom-right
        ||  (row === rows - 1 || (row === rows - 1 && col === cols - 1))) {
        
        cell.border = true;
        cell.borderType = BORDER.BOTTOM;
        return;
    }
    
    // Border left
    if ( col === 0 ) {

        cell.border = true;
        cell.borderType = BORDER.LEFT;
        return;
    }

    // Border right
    if ( col === cols - 1 ) {

        cell.border = true;
        cell.borderType = BORDER.RIGHT;
        return;
    }

    cell.border = false;
    cell.borderType = BORDER.NONE;
}

/**
 * Establece las referencias de célula alrededor para la célula que se encuentra en un corner
 * @param {Object} cell Objeto que representa a la célula
 * @param {*} rows Filas de la matriz
 * @param {*} cols Columnas de la matriz
 */
export const setAroundReferencesOfCellCorner = (cell, rows, cols) => {

    if (!cell.corner) { return; }

    // Corner top-left
    if (cell.cornerType === CORNER.TOP_LEFT) {

        cell.references = [
            { row: rows - 1, col: cols - 1}, // Corner bottom-right
            { row: rows - 1, col: 0 }, // Corner bottom-left
            { row: rows - 1, col: 1 },
            { row: 0, col: 1 },
            { row: 1, col: 1 },
            { row: 1, col: 0 },
            { row: 1, col: cols - 1 },
            { row: 0, col: cols - 1}, // Corner top-right            
        ];
        return;
    }

    // Corner top-right
    if (cell.cornerType === CORNER.TOP_RIGHT) {

        cell.references = [
            { row: rows - 1, col: cols - 2},
            { row: rows - 1, col: cols - 1 }, // Corner bottom-right
            { row: rows - 1, col: 0 }, // Corner bottom-left
            { row: 0, col: 0 }, // Corner top-left
            { row: 1, col: 0 },
            { row: 1, col: cols - 1 },
            { row: 1, col: cols - 2 },
            { row: 0, col: cols - 2 },           
        ];
        return;
    }

    // Corner bottom-left
    if (cell.cornerType === CORNER.BOTTOM_LEFT) {

        cell.references = [
            { row: rows - 2, col: cols - 1},
            { row: rows - 2, col: 0 },
            { row: rows - 2, col: 1 },
            { row: rows - 1, col: 1 },
            { row: 0, col: 1 },
            { row: 0, col: 0 },
            { row: 0, col: cols - 1}, // Corner top-right
            { row: rows - 1, col: cols - 1 }, // Corner bottom-right          
        ];
        return;
    }

    // Corner bottom-right
    if (cell.cornerType === CORNER.BOTTOM_RIGHT) {

        cell.references = [
            { row: rows - 2, col: cols - 2 },
            { row: rows - 2, col: cols - 1 },
            { row: rows - 2, col: 0 },
            { row: rows - 1, col: 0 }, // Corner bottom-left
            { row: 0, col: 0 }, // Corner top-left
            { row: 0, col: cols - 1 }, // Corner top-right
            { row: 0, col: cols - 2},
            { row: rows - 1, col: cols - 2 },
        ];
        return;
    }
}

/**
 * Establece las referencias de célula alrededor para la célula que se encuentra en un borde, pero no corner
 * @param {Object} cell Objeto que representa a la célula
 * @param {*} rows Filas de la matriz
 * @param {*} cols Columnas de la matriz
 */
export const setAroundReferencesOfCellBorder = (cell, rows, cols) => { 

    if ( cell.corner ) { return; }
    if ( !cell.border ) { return; }

    if (cell.borderType === BORDER.TOP) {

        cell.references = [
            { row: rows - 1, col: cell.col - 1},
            { row: rows - 1, col: cell.col },
            { row: rows - 1, col: cell.col + 1 },
            { row: cell.row, col: cell.col + 1 },
            { row: cell.row + 1, col: cell.col + 1 },
            { row: cell.row + 1, col: cell.col },
            { row: cell.row + 1, col: cell.col - 1},
            { row: cell.row, col: cell.col - 1 },
        ];
    }

    if (cell.borderType === BORDER.BOTTOM) {

        cell.references = [
            { row: cell.row - 1, col: cell.col - 1},
            { row: cell.row - 1, col: cell.col },
            { row: cell.row - 1, col: cell.col + 1 },
            { row: cell.row, col: cell.col + 1 },
            { row: 0, col: cell.col + 1 },
            { row: 0, col: cell.col },
            { row: 0, col: cell.col - 1},
            { row: cell.row, col: cell.col - 1 },
        ];
    }

    if (cell.borderType === BORDER.LEFT) {

        cell.references = [
            { row: cell.row - 1, col: cols - 1},
            { row: cell.row - 1, col: cell.col },
            { row: cell.row - 1, col: cell.col + 1 },
            { row: cell.row, col: cell.col + 1 },
            { row: cell.row + 1, col: cell.col + 1 },
            { row: cell.row + 1, col: cell.col },
            { row: cell.row + 1, col: cols - 1},
            { row: cell.row, col: cols - 1 },
        ];
    }

    if (cell.borderType === BORDER.RIGHT) {

        cell.references = [
            { row: cell.row - 1, col: cols - 2},
            { row: cell.row - 1, col: cell.col },
            { row: cell.row - 1, col: 0 },
            { row: cell.row, col: 0 },
            { row: cell.row + 1, col: 0 },
            { row: cell.row + 1, col: cell.col },
            { row: cell.row + 1, col: cell.col - 1},
            { row: cell.row, col: cell.col - 1 },
        ];
    }
}

/**
 * Establece las referencias de célula alrededor para la célula que no es borde ni corner
 * @param {Object} cell Objeto que representa a la célula
 * @param {*} rows Filas de la matriz
 * @param {*} cols Columnas de la matriz
 */
export const setAroundReferencesOfCell = (cell, rows, cols) => { 

    if ( cell.corner || cell.border ) { return; }

    cell.references = [
        { row: cell.row - 1, col: cell.col - 1},
        { row: cell.row - 1, col: cell.col },
        { row: cell.row - 1, col: cell.col + 1 },
        { row: cell.row, col: cell.col + 1 },
        { row: cell.row + 1, col: cell.col + 1 },
        { row: cell.row + 1, col: cell.col },
        { row: cell.row + 1, col: cell.col - 1},
        { row: cell.row, col: cell.col - 1},
    ];
}

/**
 * Determina el estado de la célula de acuerdo a las células que la rodean
 * @param {Object} cell Célula a determinar su estado
 * @param {Function} setCell Función que modifica el estado de la célula
 * @param {Array} references Array con las células que rodean a la célula a evaluarse
 */
export const checkStatusCell = (cell, setCell, references) => {

    const alives = references.filter(ref => ref.status === STATUS_CELL.ALIVE ).length;

    // Si la célula está viva
    if (cell.status === STATUS_CELL.ALIVE) {
    
        // Una célula viva con menos de 2 células vecinas vivas muere de “soledad”
        // Una célula viva con más de 3 células vecinas vivas muere por “sobrepoblación”
        if (alives < 2  || alives > 3 ) {
            
            setCell({
                ...cell,
                status: STATUS_CELL.DEAD
            });
        }        
    }
    // Si la célula está muerta
    else {

        // Una célula muerta con exactamente 3 células vivas vecinas, “nace”
        if ( alives === 3 ) {

            setCell({
                ...cell,
                status: STATUS_CELL.ALIVE
            });
        }
    }
    
}