import { useEffect, useState } from 'react';

// Environments
import { environment } from '../environments/environment';

// Helpers
import { fillArray } from '../helpers';

let generations = 0;
let initInterval;
export const useBoard = (rows, cols) => {

    const [cells, setCells] = useState(fillArray(rows, cols));
    const [count, setCount] = useState(0);
    const [started, setStarted] = useState(false);
    const [loading, setLoading] = useState(false);

    const handleStart = () => {

        if ( started ) { return; }

        setStarted(true);
        initInterval = setInterval(() => {
            
            setCount(generations++);

        }, environment.timeForGeneration);
    }

    const handleStop = () => {

        if ( !started ) { return; }

        setStarted(false);
        clearInterval(initInterval);
    }

    const handleReset = () => {

        clearInterval(initInterval);
        generations = 0;
        setCount(0);
        setStarted(false);
        
        setLoading(true);
        setCells(fillArray(rows, cols));
    }

    // Cuando el componente se desmonta, necesito limpiar el timer
    useEffect(() => {      
        
      return () => {

        clearInterval(initInterval);
      }
    }, []);
    

    useEffect(() => {

        setLoading(false);
        
    }, [cells]);

    return {

        handleStart,
        handleStop,
        handleReset,
        cells, 
        setCells,
        count,
        started,
        loading
    }
}
