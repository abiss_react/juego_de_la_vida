export const LOCATION = {
    
    NONE: 'None',
    TOP: 'Top',
    LEFT: 'Left',
    BOTTOM: 'Bottom',
    RIGHT: 'Right'
}