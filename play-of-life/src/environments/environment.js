
export const environment = {

    timeForGeneration: process.env.REACT_APP_TIME_FOR_GENERATION,
    matriz: {
        rows: Number(process.env.REACT_APP_MATRIZ_ROWS),
        cols: Number(process.env.REACT_APP_MATRIZ_COLS)
    }
};