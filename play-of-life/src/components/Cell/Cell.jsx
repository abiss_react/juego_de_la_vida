import { useContext, useEffect, useState } from 'react';

// Contexts
import { BoardContext } from '../../contexts/board.context';

// Hooks
import { useCell } from '../../hooks/useCell';

import './Cell';

export const Cell = ({

  row = 0,
  col = 0,
}) => {

  // const [color, setColor] = useState({ backgroundColor: 'gray' });
  const { started, handleStop } = useContext(BoardContext);
  const { cell, changeStatus, color, resolveColor } = useCell(row, col);

  // Cuando presiona sobre una célula
  const handleClick = () => {

    if (started) {
      handleStop();
    }

    resolveColor(cell);
    changeStatus();
  }

  // Observa los cambios sobre la célula
  useEffect(() => {

    if (started) {

      resolveColor(cell);
      changeStatus();
    }

  }, [cell]);

  useEffect(() => {

    if (started) {

      resolveColor(cell);
    }

  }, [started]);

  return <>
    <div
      style={{ ...color }}
      onClick={handleClick}
      className="circle"
    >
    </div>
  </>
};
