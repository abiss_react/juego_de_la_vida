import React from 'react';
import { Button, Col, Row } from 'react-bootstrap';

// Contexts
import { BoardContext } from '../../contexts/board.context';

// Environments
import { environment } from '../../environments/environment';

// Hooks
import { useBoard } from '../../hooks/useBoard';
import { Cell } from '../Cell/Cell';

export const Board = () => {

  const { rows, cols } = environment.matriz;
  const {
    handleStart,
    handleStop,
    handleReset,
    cells,
    setCells,
    count,
    started,
    loading
  } = useBoard(rows, cols);

  return (
    <BoardContext.Provider
      value={{
        cells,
        setCells,
        started,
        count,
        handleStop
      }}
    >
      <div>
        <h1>Board</h1>
        <Row className="mx-0">
          <Button as={Col} variant={started ? 'secondary' : 'primary'} onClick={handleStart}>Iniciar</Button>
          <Button as={Col} variant={!started ? 'secondary' : 'primary'} className="mx-2" onClick={handleStop}>Detener</Button>
          <Button as={Col} variant="success" onClick={handleReset}>Reiniciar</Button>
          <h4 as={Col}>Generación: {count}</h4>
        </Row>
        <table style={{margin: 'auto'}}>
          <tbody>
            {
              loading && <h1>Cargando...</h1>
            }
            {
              !loading &&
              <>
                {
                  cells.map((row, i) => {

                    return (
                      <tr key={'row_' + i}>
                        {
                          row.map((col, j) => (
                            <td key={'col_' + j}>
                              <Cell
                                row={i}
                                col={j}
                              />
                            </td>
                          ))
                        }
                      </tr>
                    );
                  })
                }
              </>
            }
          </tbody>
        </table>
      </div>
    </BoardContext.Provider>
  )
};
